(* open Core *)

(* open Board *)
open Ui

(* let print_row (row : Board.row) = *)
(*   print_string (to_line row) *)


(* let run_step win = *)
(*   let board = Board.create_board in *)
(*   print_board win board *)
  (* Out_channel.output_string stdout (sprintf "%d" (calc_neighbours board 19 19)) *)
  (* print_endline (sprintf "%d" (get board 0 0)) *)

(* let min x1 x2 = if x1 < x2 then x1 else x2 *)

let usage_msg = "-i iterations"
let iterations = ref 10
let input_files = ref []

let anon_fun filename = input_files := filename :: !input_files
let speclist =
  [
    ("-i", Arg.Set_int iterations, "Number of iterations");
  ]

let rec run_iter board_window i board =
  match i with
  | i when i == !iterations -> ()
  | _ -> let _ = print_board board_window i board in
    let new_board = Board.iterate board in
    Unix.sleep 1;
    run_iter board_window (i+1) new_board

let () =
  let _ = Arg.parse speclist anon_fun usage_msg in
  let board = Board.create_board in
  let board_window = init_screen "Conway's Game of Life" in
  let _ = run_iter board_window 1 board in
  shutdown_screen ()
